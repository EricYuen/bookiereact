// @flow
import {Navigation} from 'react-native-navigation';

import MainView from '../components/MainView';
import SideMenu from '../components/SideMenu';
import RecordsView from '../components/RecordsView';
import CreateUserView from '../components/CreateUserView';
import LoginView from '../components/LoginView';
import AccountView from '../components/AccountView';
import SendFeedbackView from '../components/SendFeedbackView';
import InviteView from '../components/InviteView';

export const SCREEN_MAIN = 'com.bookiereact.MainView';
export const SCREEN_SIDE_MENU = 'com.bookiereact.SideMenu';
export const SCREEN_RECORDS = 'com.bookiereact.RecordsView';
export const SCREEN_CREATE_USER = 'com.bookiereact.CreateUserView';
export const SCREEN_LOGIN = 'com.bookiereact.LoginView';
export const SCREEN_ACCOUNT = 'com.bookiereact.AccountView';
export const SCREEN_FEEDBACK = 'com.bookiereact.SendFeedback';
export const SCREEN_INVITE = 'com.bookiereact.InviteView';

// register all screens of the app (including internal ones)
export function registerScreens(store: Object, Provider: Object) {
  Navigation.registerComponent(SCREEN_MAIN, () => MainView, store, Provider);
  Navigation.registerComponent(SCREEN_SIDE_MENU, () => SideMenu, store, Provider);
  Navigation.registerComponent(SCREEN_RECORDS, () => RecordsView, store, Provider);
  Navigation.registerComponent(SCREEN_CREATE_USER, () => CreateUserView, store, Provider);
  Navigation.registerComponent(SCREEN_LOGIN, () => LoginView, store, Provider);
  Navigation.registerComponent(SCREEN_ACCOUNT, () => AccountView, store, Provider);
  Navigation.registerComponent(SCREEN_FEEDBACK, () => SendFeedbackView, store, Provider);
  Navigation.registerComponent(SCREEN_INVITE, () => InviteView, store, Provider);
}
