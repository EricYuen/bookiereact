/**
 * PinchLocator
 * @flow
 */

import Locator from './Locator';
import GeofireWrapper from './GeofireWrapper';
import UserLoader from './UserLoader';
import * as Analytics from './Analytics';
import * as locationActions from '../actions/locationActions';

const USER_PROXIMITY_RADIUS = 0.1; // In kilometers

var currentUserID = null;
var watchID = null;
var updateNearby = null;
var reduxStore = null;

var enteredNearbyUser = function(key, location, distance) {
  // Ignore user if it's self
  if (key === currentUserID) {
    return;
  }
  Analytics.logEvent(Analytics.EVENT_USER_ENTERED, {
    latitude: location[0],
    longitude: location[1],
    distance: distance
  });

  // Might need to load the user for the first time
  UserLoader.loadUser(key);

  // Update the user's location
  let locationCopy = {
    latitude: location[0],
    longitude: location[1],
    distance
  };
  UserLoader.updateLocation(key, locationCopy);
};

var exitedNearbyUser = function(key, location, distance) {
  // Ignore user if it's self
  if (key === currentUserID) {
    return;
  }
  Analytics.logEvent(Analytics.EVENT_USER_EXITED, {
    latitude: location[0],
    longitude: location[1],
    distance: distance
  });

  // Update the user's location
  let locationCopy = {
    latitude: location[0],
    longitude: location[1],
    distance
  };
  UserLoader.updateLocation(key, locationCopy);
};

export function init(store) {
  reduxStore = store;
}

export function setCurrentUserID(userID = null) {
  currentUserID = userID;
}

export function startLocationTracking() {
  if (!watchID) {
    Locator.config();
    watchID = Locator.startLocationUpdates((location) => {
      GeofireWrapper.updateLocation(currentUserID, [location.latitude, location.longitude]);
      reduxStore.dispatch(locationActions.updateLocation(location));

      startListening(location);
    }, (error) => {
      // Handle error
      Analytics.logEvent(Analytics.EVENT_NO_LOCATION);
      reduxStore.dispatch(locationActions.locationError(error));
    });
  }
}

export function stopLocationTracking() {
  if (watchID) {
    Locator.stopLocationUpdates(watchID);
    watchID = null;
    stopListening();
  }
}

export function startCurrentLocationListening() {
  return Locator.getCurrentLocation().then((location) => {
    startListening(location);
  }).catch((error) => {
    Analytics.logEvent(Analytics.EVENT_NO_LOCATION);
    reduxStore.dispatch(locationActions.locationError(error));
  });
}

export function startListening(location) {
  // Get users nearby
  if (!updateNearby) {
    // Get an realtime list of nearby [latitude, longitude] with radius in km
    updateNearby = GeofireWrapper.updateNearbyUsers(
      location.latitude,
      location.longitude,
      USER_PROXIMITY_RADIUS,
      enteredNearbyUser,
      exitedNearbyUser
    );
  } else {
    GeofireWrapper.modifyUpdateNearbyUsers(updateNearby, location.latitude, location.longitude, USER_PROXIMITY_RADIUS);
  }
}

export function stopListening() {
  if (updateNearby) {
    GeofireWrapper.cancelUpdate(updateNearby);
    updateNearby = null;
  }
}
