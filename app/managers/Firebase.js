/**
 * Firebase
 * @flow
 */
import firebase from 'react-native-firebase';
import GeoFire from 'geofire';

import Entry from '../models/Entry';
import User from '../models/User';
import { accountPairingKey } from './keyGenerators';

var instance;
var onAuthChangedCallback;

var signInChangeCallBack = function(account) {
  if (account) {
    // User is signed in.
    firebase.analytics().setUserId(account.uid);
  } else {
    // User is signed out.
    firebase.analytics().setUserId(null);
  }
  // Pass the account to the callback
  if (onAuthChangedCallback) {
    onAuthChangedCallback(account);
  }
};

export default class Firebase {

  static getInstance(): Firebase {
    if (!instance) {
      firebase.auth().onAuthStateChanged(signInChangeCallBack);
      instance = new Firebase();
    }
    return instance;
  }

  createUser(email: string, password: string) {
    return firebase.auth().createUserWithEmailAndPassword(email, password);
  }

  setOnAuthChange(authCallback: Function) {
    onAuthChangedCallback = authCallback;
  }

  getAccount() {
    return firebase.auth().currentUser;
  }

  signIn(email: string, password: string) {
    return firebase.auth().signInWithEmailAndPassword(email, password);
  }

  signOut() {
    return firebase.auth().signOut();
  }

  getGeofire() {
    let firebaseRef = firebase.database().ref('user_locations');
    return new GeoFire(firebaseRef);
  }

  getFCMToken() {
    return firebase.messaging().getToken();
  }

  updateFCMToken(userID: string, token: string) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('users').doc(userID).update({
        token
      }).then(() => {
        resolve();
      });
    });
  }

  setName(userID: string, name: string) {
    return firebase.firestore().collection('users').doc(userID).set({
      name: name
    }, {merge: true});
  }

  getUser(userID: string) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('users').doc(userID).get().then((docSnapshot) => {
        let userData = docSnapshot.data();
        let user = null;
        if (userData) {
          user = new User(userID, userData);
        }
        resolve(user);
      }).catch((e) => {
        reject(e);
      });
    });
  }

  getFriendIDs(userID: string) {
    return new Promise((resolve, reject) => {
      let friends = [];
      firebase.firestore().collection('records').where('user1', '==', userID).get().then((docsSnapshot1) => {
        firebase.firestore().collection('records').where('user2', '==', userID).get().then((docsSnapshot2) => {
          // Get the id of the other user on the record
          docsSnapshot1.forEach((docSnapshot) => {
            friends.push(docSnapshot.data().user2);
          });
          docsSnapshot2.forEach((docSnapshot) => {
            friends.push(docSnapshot.data().user1);
          });

          resolve(friends);
        }).catch((error) => {
          reject(error);
        });
      }).catch((error) => {
        reject(error);
      });
    });
  }

  addEntryToRecord(pairingKey: string, entry: Object) {
    return new Promise((resolve, reject) => {
      let batch = firebase.firestore().batch();

      let recordsRef = firebase.firestore().collection('records').doc(pairingKey);
      // Set record's user1 and user2 fields based on natural sort order
      batch.set(recordsRef, {
        user1: pairingKey.substring(0, pairingKey.indexOf('|')),
        user2: pairingKey.substring(pairingKey.indexOf('|') + 1)
      }, {merge: true});

      let historyRef = firebase.firestore().collection('records').doc(pairingKey).collection('history').doc();
      batch.set(historyRef, entry);

      batch.commit().then(() => {
          resolve();
      });
    });
  }

  getEntriesForRecord(pairingKey: string, callback: Function) {
    // Intercept the firebase response and return appropriate object
    let callbackMiddleware = function(snapshot) {
      if (snapshot.empty) {
        callback(pairingKey, null);
      } else {
        let entries = [];
        new Promise((resolve, reject) => {
          snapshot.forEach((docSnapshot) => {
            let entryObj = docSnapshot.data();
            let entry = new Entry(docSnapshot.id, entryObj);
            entries.push(entry);
          });
          resolve();
        }).then(() => {
          callback(pairingKey, entries);
        });
      }
    };

    return firebase.firestore().collection('records').doc(pairingKey).collection('history').orderBy('timestamp', 'desc').onSnapshot(callbackMiddleware);
  }

  updateEntry(pairingKey, entry) {
    return firebase.firestore().collection('records').doc(pairingKey).collection('history').doc(entry.id).update(entry);
  }

  approveEntryByUser(pairingKey: string, entry: Entry, userID: string) {
    // Set the lenderApproved or borrowerApproved based on the userID, otherwise keep original value
    let lenderApproved = entry.lender === userID ? true : entry.lenderApproved;
    let borrowerApproved = entry.borrower === userID ? true : entry.borrowerApproved;

    return firebase.firestore().collection('records').doc(pairingKey).collection('history').doc(entry.id).set({
      lenderApproved,
      borrowerApproved
    }, {merge: true});
  }

  cancelEntry(pairingKey: string, entry: Entry, cancelled = true) {
    return firebase.firestore().collection('records').doc(pairingKey).collection('history').doc(entry.id).set({
      cancelled
    }, {merge: true});
  }

  acceptInvite(inviter: string, invitee: string) {
    return new Promise((resolve, reject) => {
      return this.getUser(inviter).then((inviterUser) => {
        // If the inviter key does not exist, do nothing
        if (inviterUser === null) {
          reject();
        } else {
          let batch = firebase.firestore().batch();

          let pairingKey = accountPairingKey(inviter, invitee);
          let recordsRef = firebase.firestore().collection('records').doc(pairingKey);
          // Set record's user1 and user2 fields based on natural sort order
          batch.set(recordsRef, {
            user1: pairingKey.substring(0, pairingKey.indexOf('|')),
            user2: pairingKey.substring(pairingKey.indexOf('|') + 1)
          }, {merge: true});

          let inviteRef = firebase.firestore().collection('invites').doc(inviter).collection('invitees').doc(invitee);
          batch.set(inviteRef, {accepted: true});

          batch.commit().then(() => {
            resolve();
          });
        }
      });
    });
  }

  setBackgroundTimestamp(userID: string, timestamp: Date) {
    return firebase.firestore().collection('user_activities').doc(userID).set({
      backgroundTimestamp: timestamp
    }, {merge: true});
  }

  setActivityTimestamp(userID: string, timestamp: Date) {
    return firebase.firestore().collection('user_activities').doc(userID).set({
      timestamp
    }, {merge: true});
  }

  setNotificationHistory(userID: string, key: string, timestamp: Date) {
    return firebase.firestore().collection('notifications').doc(userID).collection('notification_timestamps').doc(key).set({
      timestamp
    });
  }

  getNotificationHistory(userID: string, key: string) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('notifications').doc(userID).collection('notification_timestamps').doc(key).get().then((snapshot) => {
          resolve(snapshot.exists ? snapshot.data().timestamp : null);
      });
    });
  }

  sendFeedback(userID: string, feedback: string, version: string, timestamp: Date) {
    firebase.firestore().collection('feedbacks').doc().set({
      user: userID,
      feedback,
      version,
      timestamp
    });
  }

  logEvent(event: string, params?: Object) {
    firebase.analytics().logEvent(event, params);
  }

  setCurrentScreen(screenName, screenClassOverride) {
    firebase.analytics().setCurrentScreen(screenName, screenClassOverride);
  }

  setUserId(userID) {
    firebase.analytics().setUserId(userID);
  }
}
