/**
 * Errors
 * @flow
 */

import {
  ToastAndroid
} from 'react-native';
import * as Analytics from './Analytics';

export const ERROR_EMPTY_EMAIL = 1;
export const ERROR_EMPTY_NAME = 2;
export const ERROR_EMPTY_PASSWORD = 3;

function getErrorByCode(code) {
  let error = {code};
  switch (code) {
    case ERROR_EMPTY_EMAIL:
      error.message = 'Please enter your email address.';
      break;
    case ERROR_EMPTY_NAME:
      error.message = 'Please enter your name.';
      break;
    case ERROR_EMPTY_PASSWORD:
      error.message = 'Please enter your password.';
      break;
    default:
      error.message = 'Unknown error. Please try again later.';
  }

  return error;
}

export function displayErrorByCode(code: number) {
  displayError(getErrorByCode(code));
}

export function displayError(error, duration = ToastAndroid.LONG) {
  Analytics.logEvent(Analytics.EVENT_SHOW_ERROR, {error: error.message});

  ToastAndroid.show(error.message, duration);
}
