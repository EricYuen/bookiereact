/**
 * GeofireWrapper
 * @flow
 */
import Firebase from './Firebase';

export default class GeofireWrapper {

  static updateLocation(key: string, location: Array) {
    let geoFire = Firebase.getInstance().getGeofire();
    return geoFire.set(key, location).then(() => {
      return Firebase.getInstance().setActivityTimestamp(key, new Date());
    });
  }

  static updateNearbyUsers(latitude: number, longitude: number, radius: number, enteredCallback: Function, exitedCallback: Function) {
    let geoFire = Firebase.getInstance().getGeofire();
    var geoQuery = geoFire.query({
      center: [latitude, longitude],
      radius: radius
    });

    geoQuery.on('key_entered', enteredCallback);
    geoQuery.on('key_exited', exitedCallback);

    return geoQuery;
  }

  static modifyUpdateNearbyUsers(geoQuery: Object, latitude: number, longitude: number, radius: number) {
    geoQuery.updateCriteria({
      center: [latitude, longitude],
      radius: radius
    });
  }

  static cancelUpdate(geoQuery: Object) {
    if (geoQuery) {
      geoQuery.cancel();
    }
  }

}
