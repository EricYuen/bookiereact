/**
 * DeepLinking
 * @flow
 */
import {
  Linking
 } from 'react-native';
 import * as Analytics from './Analytics';
 import * as invitesActions from '../actions/invitesActions';

var reduxStore = null;

var parseQueryString = function(queryString) {
    let params = {};
    // Split into key/value pairs
    let queries = queryString.split('&');
    // Convert the array of strings into an object
    for (let i = 0; i < queries.length; i++ ) {
        let temp = queries[i].split('=');
        params[temp[0]] = temp[1];
    }
    return params;
};

export default class DeepLinking {

  static init(store) {
    reduxStore = store;
  }

  static install() {
    Linking.getInitialURL().then(this.appLaunchWithURL);
    Linking.addEventListener('url', this.handleOpenURL);
  }

  static uninstall() {
     Linking.removeEventListener(this.handleOpenURL);
  }

  static handleOpenURL(event) {
    if (event) {
      DeepLinking.appLaunchWithURL(event.url);
    }
  }

  static appLaunchWithURL(url: string) {
    if (!url) {
      return;
    }

    // Parse the url and query strings eg. http://pinchit.ca/invite?code=<userID>
    let queryString = url.substring( url.indexOf('?') + 1 );
    let params = parseQueryString(queryString);

    let userID = params.code;
    if (userID) {
      Analytics.logEvent(Analytics.EVENT_OPENED_INVITE);
      reduxStore.dispatch(invitesActions.addInvite(userID));
    }
  }

}
