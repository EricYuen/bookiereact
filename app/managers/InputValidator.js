/**
 * InputValidator
 * @flow
 */

export function isStringEmpty(str: string) {
  return !str || !str.trim();
}
