/**
 * Locator
 * @flow
 */
import FusedLocation from 'react-native-fused-location';
import * as Permissions from './Permissions';

export default class Locator {

  static config() {
    // If running in development mode (ie. emulator), battery consumption not an issue
    if (__DEV__) {
      FusedLocation.setLocationPriority(FusedLocation.Constants.HIGH_ACCURACY);
      FusedLocation.setLocationInterval(2000);
      FusedLocation.setFastestLocationInterval(1000);
    }
  }

  static hasPermission() {
    return Permissions.hasPermission();
  }

  static requestPermission() {
    return Permissions.requestPermission();
  }

  static getCurrentLocation(force: boolean = false) {
    return FusedLocation.getFusedLocation(force);
  }

  // Callback: function(location)
  static startLocationUpdates(successCallback: Function, errorCallback: Function) {
    FusedLocation.startLocationUpdates();

    let errorSub = FusedLocation.on('fusedLocationError', (error) => {
      errorCallback(error);
    });
    let successSub = FusedLocation.on('fusedLocation', (location) => {
      successCallback(location);
    });

    return {successSub, errorSub};
  }

  static stopLocationUpdates(updateSubs: Object) {
    FusedLocation.off(updateSubs.successSub);
    FusedLocation.off(updateSubs.errorSub);
    FusedLocation.stopLocationUpdates();
  }
}
