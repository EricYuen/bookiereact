/**
 * PersistentStore
 * @flow
 */

import {
   AsyncStorage
} from 'react-native';

const USER_ID = 'USER_ID';

export default class PersistentStore {

  static saveUserID(userID: string) {
    return AsyncStorage.setItem(USER_ID, userID);
  }

  static removeUserID() {
    return AsyncStorage.removeItem(USER_ID);
  }

  static getUserID(){
    return AsyncStorage.getItem(USER_ID);
  }
}
