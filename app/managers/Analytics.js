/**
 * Analytics
 * @flow
 */

import Firebase from './Firebase';

// Onboarding events
export const EVENT_VIEW_LOGIN = 'view_login';
export const EVENT_LOGIN_PRESSED = 'login_pressed';
export const EVENT_LOGIN = 'login';
export const EVENT_LOGIN_FAILED = 'login_failed';
export const EVENT_VIEW_SIGN_UP = 'view_sign_up';
export const EVENT_SIGN_UP_PRESSED = 'sign_up_pressed';
export const EVENT_SIGN_UP = 'sign_up';
export const EVENT_SIGN_UP_FAILED = 'sign_up_failed';

// Records view events
export const EVENT_NO_LOCATION = 'no_location';
export const EVENT_PERMISSION_DENIED = 'location_permission_denied';
export const EVENT_USER_ENTERED = 'user_entered';
export const EVENT_USER_EXITED = 'user_exited';
export const EVENT_VIEW_ACCOUNT = 'view_account';
export const EVENT_CLOSE_ACCOUNT = 'close_account';

// Account view events: creator events
export const EVENT_LEND_PRESSED = 'lend_pressed';
export const EVENT_LEND_CREATED = 'lend_created';
export const EVENT_LEND_CANCELLED = 'lend_cancelled';
export const EVENT_BORROW_PRESSED = 'borrow_pressed';
export const EVENT_BORROW_CREATED = 'borrow_created';
export const EVENT_BORROW_CANCELLED = 'borrow_cancelled';

// Account view events: receiver events
export const EVENT_LEND_CONFIRMED = 'lend_confirmed';
export const EVENT_LEND_REJECTED = 'lend_rejected';
export const EVENT_BORROW_CONFIRMED = 'borrow_confirmed';
export const EVENT_BORROW_REJECTED = 'borrow_rejected';

// Side menu events
export const EVENT_OPEN_SIDE_MENU = 'open_side_menu';
export const EVENT_LOGOUT = 'logout';
export const EVENT_VIEW_SEND_FEEDBACK = 'view_send_feedback';
export const EVENT_SEND_FEEDBACK = 'send_feedback';

// Invites
export const EVENT_OPENED_INVITE = 'open_invite';
export const EVENT_SHARE_OPENED = 'open_share';
export const EVENT_SHARE_TAPPED = 'tap_share';
export const EVENT_SHARE_SUCCEED = 'shared_succeed';
export const EVENT_SHARE_DISMISSED = 'shared_dismissed';

export const EVENT_SHOW_ERROR = 'show_error';

// Parameter values
export const PARAM_EMAIL = 'email';

export function logEvent(event: string, params?: Object) {
  Firebase.getInstance().logEvent(event, params);
}

export function setCurrentScreen(screenName, screenClassOverride) {
  Firebase.getInstance().setCurrentScreen(screenName, screenClassOverride);
}

export function setUserId(userID) {
  Firebase.getInstance().setUserId(userID);
}
