/**
 * UserLoader
 * @flow
 */
import Firebase from './Firebase';
import User from '../models/User';

import * as recordsActions from '../actions/recordsActions';
import { accountPairingKey, getValuesFromDictionary } from '../managers/keyGenerators';
import * as PinchLocator from '../managers/PinchLocator';

var store;
var entriesUnsubscribes; // {accountPairingKey: unsubscribe, ...}

export default class UserLoader {

  static init(reduxStore: Object) {
    store = reduxStore;
    entriesUnsubscribes = {};
  }

  static loadFriends(userID: string) {
    return Firebase.getInstance().getFriendIDs(userID).then((friends) => {
      UserLoader.loadUsers(friends);
    });
  }

  static loadUser(key: string, location: Object = null, distance: number = Number.MAX_SAFE_INTEGER) {
    if (store.getState().records.records[key]) {
      // Force a store dispatch to update component props
      let existingRecord = store.getState().records.records[key];
      store.dispatch(recordsActions.addRecord(existingRecord));
      return;
    }

    Firebase.getInstance().getUser(key).then((user: User) => {
      if (store.getState().user.user) {
        UserLoader.loadEntries(store.getState().user.user.id, key);
      }

      // Store the record in redux
      store.dispatch(recordsActions.addRecord(user));
    });
  }

  static loadUsers(keys: Array) {
    for (let i = 0; i < keys.length; i++ ) {
      let key = keys[i];
      UserLoader.loadUser(key);
    }
  }

  static updateLocation(key: string, location: Object) {
    store.dispatch(recordsActions.updateRecordLocation(key, location));
  }

  static loadEntries(currentUserID: string, key: string) {
    let pairingKey = accountPairingKey(currentUserID, key);
    if (!entriesUnsubscribes[pairingKey]) {
      entriesUnsubscribes[pairingKey] = Firebase.getInstance().getEntriesForRecord(pairingKey, (pairingKeyParam: string, entries: Array) => {
        UserLoader.refreshEntries(pairingKeyParam, entries);
      });
    }
  }

  static unloadEntries(currentUserID: string, key: string) {
    let pairingKey = accountPairingKey(currentUserID, key);
    if (entriesUnsubscribes[pairingKey]) {
      entriesUnsubscribes[pairingKey]();
      delete entriesUnsubscribes[pairingKey];
    }
  }

  static refreshEntries(pairingKey: string, entries: Array) {
    if (store.getState().user) {
      // Find the other user's key from the pairingKey
      let userID;
      if (pairingKey.indexOf(store.getState().user.user.id) === 0){
        userID = pairingKey.substring(pairingKey.indexOf('|') + 1);
      } else {
        userID = pairingKey.substring(0, pairingKey.indexOf('|'));
      }

      store.dispatch(recordsActions.updateRecordEntries(userID, entries));
    }
  }

  static removeUser(userID: string) {
    // Remove the record from redux
    store.dispatch(recordsActions.removeRecord(userID));
    if (store.getState().user.user) {
      UserLoader.unloadEntries(store.getState().user.user.id, userID);
    }
  }

  static clearAll() {
    store.dispatch(recordsActions.clearRecords());
    // Call all entries unsubscribes
    let entriesUnsubscribesArray = getValuesFromDictionary(entriesUnsubscribes);
    entriesUnsubscribesArray.forEach((entriesUnsubscribe)=>{
      entriesUnsubscribe();
    });
    entriesUnsubscribes = {};
  }

  static refreshUserList(userID: string) {
    PinchLocator.stopListening();
    UserLoader.clearAll();
    return UserLoader.loadFriends(userID).then(() => {
      // Start listening at that the current location again
      PinchLocator.startCurrentLocationListening();
    });
  }
}
