/**
 * KeyGenerator
 * @flow
 */

export function accountPairingKey(userID: string, otherUserID: string) {
  // Concatenate the two user's ID alphabetically to create a pairing key
  if (userID < otherUserID) {
    return userID + '|' + otherUserID;
  } else {
    return otherUserID + '|' + userID;
  }
}

export function getValuesFromDictionary(dictionary) {
  let values = [];
  for (var key in dictionary){
    values.push(dictionary[key]);
  }
  return values;
}
