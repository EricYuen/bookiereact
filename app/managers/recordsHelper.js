/**
 * recordsHelper
 * @flow
 */

const LOCATION_LISTENING_TOLERANCE = 50; // In meters

// Sort Users by names
export function sortUsersByName(users: Array) {
  return users.sort((a,b) => {
    let nameA = a.name.toUpperCase(); // ignore upper and lowercase
    let nameB = b.name.toUpperCase(); // ignore upper and lowercase
    if (nameA < nameB) {
     return -1;
    }
    if (nameA > nameB) {
     return 1;
    }

    // names must be equal
    return 0;
  });
}

export function sortUsersByDistance(users: Array, locations) {
  return users.sort((a,b) => {
    return locations[a.id].distance - locations[b.id].distance;
  });
}

// Calculate the balance of all the entries in the given array, return the balance relative to the userID
export function calculateBalance(userID: string, entries: Array) {
  // No balance if no entries
  if (!entries) {
    return 0;
  }

  let balance = 0;
  let totalLent = 0;
  let totalBorrowed = 0;
  entries.forEach((entry) => {
    // Ignore pending and cancelled entries in calculation
    if (entry.isPending() || entry.cancelled) {
      return;
    }

    if (entry.lender === userID) {
      balance += entry.amount;
      totalLent += entry.amount;
    } else if (entry.borrower === userID) {
      balance -= entry.amount;
      totalBorrowed -= entry.amount;
    }
  });

  return { balance, totalLent, totalBorrowed };
}


export function hasPending(entries: Array) {
  let foundPending = false;

  // No entries exist, therefore no pending
  if (!entries) {
    return foundPending;
  }

  for (let entry of entries) {
    // Entry that was cancelled is not pending
    if (entry.isPending() && !entry.cancelled) {
      foundPending = true;
      break;
    }
  }

  return foundPending;
}

export function isNearby(location) {
  if (location && location.hasOwnProperty('distance')) {
    return location.distance < LOCATION_LISTENING_TOLERANCE;
  }
  return false;
}

// Formats a float to a string
export function formatBalance(balance, includeSign = false) {
  let sign = '';
  if (includeSign && balance !== 0) {
    sign = balance > 0 ? '+ ' : '- ';
  }

  return sign + '$' + Math.abs(balance).toFixed(2);
}

// Formats a string to insure that it's $xxx.xx
export function formatAmountInput(amount) {
  // If the text already includes a $ sign in the front, leave it, otherwise append to front
  amount = amount.charAt(0) === '$' ? amount : '$' + amount;
  // If there is more than one period, and the last character is another period, remove last one
  let numbers = amount.split('.');
  if (numbers.length > 2 && amount.charAt(amount.length - 1)) {
    amount = amount.substring(0, amount.length - 1);
  }
  // If amount contains a decimal point, do not allow more than 3 numbers right of decimal
  if (numbers.length === 2 && numbers[1].length > 2) {
    amount = amount.substring(0, amount.indexOf('.') + 3);
  }
  console.log(amount);
  return amount;
}
