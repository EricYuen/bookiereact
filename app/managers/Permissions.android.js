/**
 * Permissions
 * @flow
 */
import { PermissionsAndroid } from 'react-native';

export function hasPermission() {
  return PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION)
    .then((grantedFine) => {
      if (grantedFine) {
        return PermissionsAndroid.check(PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION);
      } else {
        return false;
      }
  });
}

export function requestPermission() {
  return PermissionsAndroid.requestMultiple([
      PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
      PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION
    ]).then((permissions) => {
      let fine = permissions[PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION];
      let coarse = permissions[PermissionsAndroid.PERMISSIONS.ACCESS_COARSE_LOCATION];
      return fine === 'granted' && coarse === 'granted';
    });
}
