/**
 * User
 * @flow
 */

import Model from './Model';

export default class User extends Model {

  constructor(id, jsonObject) {
    super(jsonObject);
    this.id = id;
  }
}
