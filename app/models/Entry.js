/**
 * Entry
 * @flow
 */

import Model from './Model';

export default class Entry extends Model {
  id: string;
  lender: string;
  borrower: string;
  amount: number;
  note: string;
  timestamp: Date;
  lenderApproved: boolean;
  borrowerApproved: boolean;
  cancelled: boolean;

  constructor(id, jsonObject) {
    super(jsonObject);
    this.id = id;
  }

  init(lender, borrower, amount, timestamp) {
    this.lender = lender;
    this.borrower = borrower;
    this.amount = amount;
    this.timestamp = timestamp;

    // Defaults
    this.note = null;
    this.lenderApproved = false;
    this.borrowerApproved = false;
  }

  approveByUser(userID: string) {
    if (userID === this.lender) {
      this.lenderApproved = true;
    }
    if (userID === this.borrower) {
      this.borrowerApproved = true;
    }
  }

  isPending() {
    return !this.lenderApproved || !this.borrowerApproved;
  }

  needsApprovalFromUser(userID: string) {
    if (userID === this.lender && !this.lenderApproved) {
      return true;
    } else if (userID === this.borrower && !this.borrowerApproved) {
      return true;
    }

    return false;
  }
}
