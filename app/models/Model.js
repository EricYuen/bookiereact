/**
 * Model
 * @flow
 */

export default class Model {
  constructor(json: Object) {
    Object.assign(this, json);
  }
}
