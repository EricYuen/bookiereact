// @flow
import { ActionTypes, type Action } from '../actions/actionDefines';

const initialState = {
  user: null
};

export default function updateUser(state: Object = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.SIGN_IN:
      return {
        ...state,
        user: action.user
      };
    case ActionTypes.SIGN_OUT:
      return {
        ...state,
        ...initialState
      };
    default:
      return state;
  }
}
