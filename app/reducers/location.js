// @flow
import { ActionTypes, type Action } from '../actions/actionDefines';

const initialState = {
  lastLocation: null,
  locationError: null
};

export default function locationState(state: Object = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.UPDATE_LOCATION: {
      return {
        ...state,
        lastLocation: action.location,
        locationError: null
      };
    }
    case ActionTypes.LOCATION_ERROR: {
      return {
        ...state,
        locationError: action.error
      };
    }
    case ActionTypes.LOCATION_DENIED: {
      return {
        ...state,
        locationError: action.error
      };
    }
    default:
      return state;
  }
}
