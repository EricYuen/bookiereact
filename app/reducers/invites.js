// @flow
import { ActionTypes, type Action } from '../actions/actionDefines';

const initialState = {
  invites: {} // {inviterUserID: true, ...}
};

export default function locationState(state: Object = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.ADD_INVITE: {
      let invitesCopy = {...state.invites};
      invitesCopy[action.userID] = true;
      return {
        ...state,
        invites: invitesCopy
      };
    }
    case ActionTypes.CLEAR_INVITE: {
      let invitesCopy = {...state.invites};
      delete invitesCopy[action.userID];
      return {
        ...state,
        invites: invitesCopy
      };
    }
    default:
      return state;
  }
}
