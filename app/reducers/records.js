// @flow
import { ActionTypes, type Action } from '../actions/actionDefines';

const initialState = {
  records: {}, // {userID: User, ...}
  allLocations: {}, // {userID: Location, ...}
  allEntries: {} // {userID: [Entry,...], ...}
};

export default function friendsState(state: Object = initialState, action: Action) {
  switch (action.type) {
    case ActionTypes.ADD_RECORD: {
      let recordsCopy = {...state.records};
      recordsCopy[action.user.id] = action.user;
      return {
        ...state,
        records: recordsCopy
      };
    }
    case ActionTypes.UPDATE_RECORD_LOCATION: {
      let allLocationsCopy = {...state.allLocations};
      if (action.location === null) {
        delete allLocationsCopy[action.userID];
      } else {
        allLocationsCopy[action.userID] = action.location;
      }
      return {
        ...state,
        allLocations: allLocationsCopy
      };
    }
    case ActionTypes.UPDATE_ENTRIES: {
      let allEntriesCopy = {...state.allEntries};
      if (action.entries === null) {
        delete allEntriesCopy[action.userID];
      } else {
        allEntriesCopy[action.userID] = action.entries;
      }
      return {
        ...state,
        allEntries: allEntriesCopy
      };
    }
    case ActionTypes.REMOVE_RECORD: {
      let recordsCopy = {...state.records};
      delete recordsCopy[action.userID];
      let allLocationsCopy = {...state.allLocations};
      delete allLocationsCopy[action.userID];
      let allEntriesCopy = {...state.allEntries};
      delete allEntriesCopy[action.userID];
      return {
        ...state,
        records: recordsCopy,
        allLocations: allLocationsCopy,
        allEntries: allEntriesCopy
      };
    }
    case ActionTypes.CLEAR_RECORDS: {
      return {
        ... state,
        ...initialState
      };
    }
    default:
      return state;
  }
}
