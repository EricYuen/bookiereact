/**
 * Redux reducer index file
 * @flow
 */

import user from './user';
import records from './records';
import location from './location';
import invites from './invites';

export {
  user,
  records,
  location,
  invites
};
