// @flow

import { ActionTypes } from './actionDefines';

export function addInvite(userID: string) {
  return {
    type: ActionTypes.ADD_INVITE,
    userID
  };
}

export function clearInvite(userID: string) {
  return {
    type: ActionTypes.CLEAR_INVITE,
    userID
  };
}
