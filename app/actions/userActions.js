// @flow

import { ActionTypes } from './actionDefines';

import User from '../models/User';

export function signIn(user: User) {
  return {
    type: ActionTypes.SIGN_IN,
    user
  };
}

export function signOut() {
  return {
    type: ActionTypes.SIGN_OUT
  };
}
