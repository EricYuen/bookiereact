// @flow

import { ActionTypes } from './actionDefines';

import User from '../models/User';

export function addRecord(user: User) {
  return {
    type: ActionTypes.ADD_RECORD,
    user
  };
}

export function updateRecordLocation(userID: string, location) {
  return {
    type: ActionTypes.UPDATE_RECORD_LOCATION,
    userID,
    location
  };
}

export function updateRecordEntries(userID: string, entries: Array) {
  return {
    type: ActionTypes.UPDATE_ENTRIES,
    userID,
    entries
  };
}

export function removeRecord(userID: string) {
  return {
    type: ActionTypes.REMOVE_RECORD,
    userID
  };
}

export function clearRecords() {
  return {
    type: ActionTypes.CLEAR_RECORDS
  };
}
