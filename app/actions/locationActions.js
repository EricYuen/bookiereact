// @flow

import { ActionTypes } from './actionDefines';

export function updateLocation(location: Object) {
  return {
    type: ActionTypes.UPDATE_LOCATION,
    location
  };
}

export function locationError(error: Object) {
  return {
    type: ActionTypes.LOCATION_ERROR,
    error
  };
}

export function locationDenied() {
  return {
    type: ActionTypes.LOCATION_DENIED,
    error: {denied: true}
  };
}
