/**
 * CreateUserView
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import Firebase from '../managers/Firebase';
import * as InputValidator from '../managers/InputValidator';
import * as Errors from '../managers/Errors';
import * as Analytics from '../managers/Analytics';
import * as Styles from './styles/Styles';
import Images from './images/Images';

const FIELD_KEY_FIRST = 'first';
const FIELD_KEY_EMAIL = 'email';
const FIELD_KEY_PASSWORD = 'password';

class CreateUserView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      name: '',
      email: '',
      password: ''
    };
    this.inputs = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.logoArea}>
          <Image source={Images.LOGO_KO}/>
        </View>
        <View style={styles.formArea}>
          <TextInput
            ref={ (input) => { this.inputs[FIELD_KEY_FIRST] = input; }}
            style={styles.textInput}
            returnKeyType={'next'}
            placeholder="First Name"
            value={this.state.name}
            onChangeText={(name) => this.setState({name})}
            onEndEditing={() => this.setState({name: this.state.name.trim()})}
            onSubmitEditing={() => this.inputs[FIELD_KEY_EMAIL].focus()}
          />
          <TextInput
            ref={ (input) => { this.inputs[FIELD_KEY_EMAIL] = input; }}
            style={styles.textInput}
            returnKeyType={'next'}
            placeholder="Email"
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
            onEndEditing={() => this.setState({email: this.state.email.trim()})}
            onSubmitEditing={() => this.inputs[FIELD_KEY_PASSWORD].focus()}
          />
          <TextInput
            ref={ (input) => { this.inputs[FIELD_KEY_PASSWORD] = input; }}
            style={styles.textInput}
            returnKeyType={'next'}
            secureTextEntry={true}
            placeholder="Password"
            onChangeText={(password) => this.setState({password})}
            onSubmitEditing={this.createAccount.bind(this)}
          />
          <TouchableOpacity style={styles.createButton} onPress={this.createAccount.bind(this)}>
            <Text style={styles.createButtonLabel}>Create Account</Text>
          </TouchableOpacity>
          <TouchableOpacity style={styles.backButton} onPress={this.goToLogin.bind(this)}>
            <Text style={styles.backButtonLabel}>Back</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  createAccount() {
    Analytics.logEvent(Analytics.EVENT_SIGN_UP_PRESSED);

    // Throw error if any of the fields are empty
    if (InputValidator.isStringEmpty(this.state.name)) {
      Errors.displayErrorByCode(Errors.ERROR_EMPTY_NAME);
      return;
    } else if (InputValidator.isStringEmpty(this.state.email)){
      Errors.displayErrorByCode(Errors.ERROR_EMPTY_EMAIL);
      return;
    } else if (InputValidator.isStringEmpty(this.state.password)){
      Errors.displayErrorByCode(Errors.ERROR_EMPTY_PASSWORD);
      return;
    }

    Firebase.getInstance().createUser(this.state.email, this.state.password).then((user) => {
      Firebase.getInstance().setName(user.uid, this.state.name).then(() => {
        Analytics.logEvent(Analytics.EVENT_SIGN_UP, {method: Analytics.PARAM_EMAIL});
        this.onCreateUserSuccess();
      });
    }).catch((error) => {
      // Handle fail to create user
      Analytics.logEvent(Analytics.EVENT_SIGN_UP_FAILED);
      Errors.displayError(error);
    });
  }

  onCreateUserSuccess() {
    // Automatically sign into the account upon successful account creation
    Firebase.getInstance().signIn(this.state.email, this.state.password).then(() => {
      this.props.navigator.pop();
    });
  }

  goToLogin() {
    this.props.navigator.pop();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white'
  },
  logoArea: {
    height: '16.5%',
    backgroundColor: Styles.Colors.PRIMARY,
    alignItems: 'center',
    justifyContent: 'center'
  },
  formArea: {
    marginTop: '7%',
    marginHorizontal: '10%'
  },
  textInput:{
    ...Styles.Components.TEXT_INPUT,
    marginBottom: '5%'
  },
  createButton: {
    ...Styles.Shapes.ROUNDED_RECT_LARGE_NO_BORDER,
    backgroundColor: Styles.Colors.SECONDARY,
    alignItems: 'center',
    justifyContent: 'center',
    paddingVertical: '5%'
  },
  createButtonLabel: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 15,
    color: 'white'
  },
  backButton: {
    alignSelf: 'center',
    justifyContent: 'center',
    marginTop: '5%',
    padding: '5%'
  },
  backButtonLabel: {
    fontFamily: Styles.Fonts.REGULAR,
    fontSize: 15,
    color: Styles.Colors.LIGHT_GREY
  }
});

export default CreateUserView;
