/**
 * LoginView
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  TouchableOpacity,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import Firebase from '../managers/Firebase';
import * as Errors from '../managers/Errors';
import * as Analytics from '../managers/Analytics';
import * as InputValidator from '../managers/InputValidator';
import * as Screens from '../screens/screens';
import * as Styles from './styles/Styles';
import * as Strings from './strings/Strings';
import Images from './images/Images';

const FIELD_KEY_EMAIL = 'email';
const FIELD_KEY_PASSWORD = 'password';

class LoginView extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: ''
    };
    this.inputs = {};
  }

  componentWillMount() {
    Analytics.logEvent(Analytics.EVENT_VIEW_LOGIN);
  }

  render() {
    return (
      <View style={styles.container}>
        <View style={styles.loginArea}>
          <Image style={styles.logo} source={Images.LOGO_KO}/>
          <TextInput
            ref={ (input) => { this.inputs[FIELD_KEY_EMAIL] = input; }}
            style={styles.textInput}
            returnKeyType={'next'}
            underlineColorAndroid="transparent"
            placeholder="Email"
            value={this.state.email}
            onChangeText={(email) => this.setState({email})}
            onEndEditing={() => this.setState({email: this.state.email.trim()})}
            onSubmitEditing={() => this.inputs[FIELD_KEY_PASSWORD].focus()}
          />
          <TextInput
            ref={ (input) => { this.inputs[FIELD_KEY_PASSWORD] = input; }}
            style={styles.textInput}
            returnKeyType={'next'}
            underlineColorAndroid="transparent"
            secureTextEntry={true}
            placeholder="Password"
            onChangeText={(password) => this.setState({password})}
            onSubmitEditing={this.onLogin.bind(this)}
          />
          <View style={styles.loginButtonWrapper}>
            <TouchableOpacity style={styles.loginButton} onPress={this.onLogin.bind(this)}>
              <Text style={styles.loginButtonLabel}>GO</Text>
            </TouchableOpacity>
          </View>
        </View>
        <TouchableOpacity style={styles.createButton} onPress={this.goToCreateAccount.bind(this)}>
          <Text style={styles.createLabel}>Create Account</Text>
        </TouchableOpacity>
        <Text style={styles.version}>{Strings.VERSION_STRING}</Text>
      </View>
    );
  }

  onLogin() {
    Analytics.logEvent(Analytics.EVENT_LOGIN_PRESSED);

    // Throw error if email or password is empty
    if (InputValidator.isStringEmpty(this.state.email)) {
      Errors.displayErrorByCode(Errors.ERROR_EMPTY_EMAIL);
      return;
    } else if (InputValidator.isStringEmpty(this.state.password)){
      Errors.displayErrorByCode(Errors.ERROR_EMPTY_PASSWORD);
      return;
    }

    Firebase.getInstance().signIn(this.state.email, this.state.password).then((user) => {
      // Do nothing
    }).catch((error) => {
      // Handle fail to sign in
      Analytics.logEvent(Analytics.EVENT_LOGIN_FAILED);
      Errors.displayError(error);
    });
  }

  goToCreateAccount() {
    Analytics.logEvent(Analytics.EVENT_VIEW_SIGN_UP);

    this.props.navigator.push({
      screen: Screens.SCREEN_CREATE_USER,
      animationType: 'fade',
      navigatorStyle: {
        navBarHidden: true,
        statusBarColor: Styles.Colors.SECONDARY
      }
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1
  },
  loginArea: {
    height: '50%',
    paddingHorizontal: '10%',
    backgroundColor: Styles.Colors.PRIMARY
  },
  logo: {
    alignSelf: 'center',
    marginVertical: '7%'
  },
  textInput:{
    ...Styles.Components.TEXT_INPUT,
    marginBottom: '5%'
  },
  loginButtonWrapper: {
    alignItems: 'center'
  },
  loginButton: {
    width: 48,
    height: 48,
    borderRadius: 24,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Styles.Colors.WHITE
  },
  loginButtonLabel: {
    color: Styles.Colors.GREY,
    justifyContent: 'center'
  },
  createButton: {
    alignItems: 'center',
    paddingVertical: '5%'
  },
  createLabel: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 14,
    color: Styles.Colors.GREY
  },
  version: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 14,
    color: Styles.Colors.LIGHT_GREY,
    alignSelf: 'center',
    position: 'absolute',
    bottom: 0,
    marginBottom: '2%'
  }
});

export default connect()(LoginView);
