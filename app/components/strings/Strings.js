/**
 * Strings
 * @flow
 */

import VersionNumber from 'react-native-version-number';

export const VERSION_STRING = 'v' + VersionNumber.appVersion;
export const SHARE_URL_BASE = 'https://pinchit.ca/invite?code=';
