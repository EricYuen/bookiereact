/**
 * MainView
 * @flow
 */

import React, { Component } from 'react';
import {
  ActivityIndicator
} from 'react-native';

import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import BackgroundJob from 'react-native-background-job';
import { BACKGROUND_JOB_KEY } from '../BackgroundLocationJob';
import * as userActions from '../actions/userActions';
import * as locationActions from '../actions/locationActions';
import * as invitesActions from '../actions/invitesActions';
import Firebase from '../managers/Firebase';
import * as Analytics from '../managers/Analytics';
import UserLoader from '../managers/UserLoader';
import PersistentStore from '../managers/PersistentStore';
import * as PinchLocator from '../managers/PinchLocator';
import Locator from '../managers/Locator';
import DeepLinking from '../managers/DeepLinking';
import User from '../models/User';
import RecordsView from './RecordsView';
import LoginView from './LoginView';

class MainView extends Component {

  constructor(props){
    super(props);
    this.state = {
      loading: true
    };
  }

  componentWillMount() {
    DeepLinking.install();
    // Listen for user authentication change
    Firebase.getInstance().setOnAuthChange((account) => {
      if (account) {
        // If user is signed in
        Analytics.logEvent(Analytics.EVENT_LOGIN, {method: Analytics.PARAM_EMAIL});
        Analytics.setUserId(account.uid);
        this.onLogin(account);
      } else {
        // User is not signed in
        Analytics.logEvent(Analytics.EVENT_LOGOUT);
        Analytics.setUserId(null);
        this.onLogout();
      }
    });
  }

  componentWillUnmount() {
    DeepLinking.uninstall();
    PinchLocator.stopLocationTracking();
  }

  componentWillReceiveProps(nextProps) {
    // If user is logged in
    if (this.props.user) {
      this.handleInvites(nextProps.invites);
    }
  }

  render() {
    if (this.state.loading) {
      return <ActivityIndicator style={{flex: 1}}/>;
    } else if (!this.props.user) {
      return <LoginView navigator={this.props.navigator}/>;
    } else {
      return <RecordsView navigator={this.props.navigator}/>;
    }
  }

  enableSideMenu(enable){
    this.props.navigator.setDrawerEnabled({
      side: 'right',
      enabled: enable
    });
  }

  onLogin(account: Object) {
    // Get the current user's name
    Firebase.getInstance().getUser(account.uid).then((user: User) => {
      this.props.userActions.signIn(user);

      // Save the current user's id
      PersistentStore.saveUserID(user.id);

      // Handle outstanding invites
      this.handleInvites(this.props.invites);

      // Initialize friend's list
      UserLoader.loadFriends(user.id);

      // Start tracking the user's location
      PinchLocator.setCurrentUserID(user.id);
      this.resolveLocationPermissions();
      this.registerForNotifications();
    }).finally(()=> {
      this.setState({loading: false});
      this.enableSideMenu(true);
    });
  }

  onLogout() {
    // Stop tracking the user's location
    PinchLocator.setCurrentUserID(null);
    this.stopBackgroundLocation();
    PinchLocator.stopLocationTracking();

    // Clear all the records from the list
    UserLoader.clearAll();

    // If user does not exist, prompt user to enter a name
    PersistentStore.removeUserID();
    this.props.userActions.signOut();

    this.setState({loading: false});
    this.enableSideMenu(false);
  }

  handleInvites(invites) {
    for (let inviter in invites) {
      // Ignore if inviter happens to be current user
      if (inviter !== this.props.user.id) {
        // Accept invite from user
        Firebase.getInstance().acceptInvite(inviter, this.props.user.id).then(() => {
          UserLoader.loadUser(inviter);
        }).catch((error) => {
          // Do nothing
        });
      }
      this.props.invitesActions.clearInvite(inviter);
    }
  }

  resolveLocationPermissions() {
    Locator.requestPermission().then((granted) =>{
      // App was granted has location permission
      if (granted) {
        PinchLocator.startLocationTracking();
        this.startBackgroundLocation();
      } else {
        // Handle case if user does not allow permission
        Analytics.logEvent(Analytics.EVENT_PERMISSION_DENIED);
        this.props.locationActions.locationDenied();
      }
    });
  }

  startBackgroundLocation() {
    // Schedule the background job
    BackgroundJob.schedule({
        jobKey: BACKGROUND_JOB_KEY,
        period: 300000, // 5 mins
        timeout: 90000, // 1.5 mins
        allowWhileIdle: true
    });
  }

  stopBackgroundLocation() {
    BackgroundJob.cancel({jobKey: BACKGROUND_JOB_KEY});
  }

  registerForNotifications() {
    Firebase.getInstance().getFCMToken().then((token) => {
      Firebase.getInstance().updateFCMToken(this.props.user.id, token);
    });
  }
}

const mapStateToProps = function(state) {
  return {
    user: state.user.user,
    invites: state.invites.invites
  };
};

const mapDispatchToProps = function(dispatch: Function) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    locationActions: bindActionCreators(locationActions, dispatch),
    invitesActions: bindActionCreators(invitesActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(MainView);
