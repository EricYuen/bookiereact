/**
 * RecordRow
 * @flow
 */

import React, { Component } from 'react';
import {
  Text,
  TouchableOpacity,
  StyleSheet,
  Image,
  View
} from 'react-native';
import { connect } from 'react-redux';

import { calculateBalance, hasPending, formatBalance } from '../managers/recordsHelper';
import * as Analytics from '../managers/Analytics';
import * as Screens from '../screens/screens';
import Images from './images/Images';
import User from '../models/User';
import * as Styles from './styles/Styles';

class RecordRow extends Component {
  props: {
    user: User,
    isUserNearby: boolean
  }

  state: {
    balance: number,
    hasPending: boolean
  }

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  componentWillMount() {
    this.restartListeners();
  }

  getInitialState() {
    return {
      balance: 0,
      hasPending: false
    };
  }

  componentDidUpdate(prevProps) {
    // If row's id has change upon row reuse
    if (prevProps.user.id !== this.props.user.id) {
      this.restartListeners();
    }
  }

  componentWillReceiveProps(nextProps) {
    // Refresh the entries if there is change
    let oldEntries = this.props.allEntries[this.props.user.id];
    let newEntries = nextProps.allEntries[this.props.user.id];
    if (JSON.stringify(oldEntries) !== JSON.stringify(newEntries)) {
      this.refreshRecordEntries(newEntries);
    }
  }

  restartListeners() {
    this.refreshRecordEntries(this.props.allEntries[this.props.user.id]);
  }

  refreshRecordEntries(entries) {
    if (entries && !entries.empty) {
      // Calculate new balance
      let { balance } = calculateBalance(this.props.userID, entries);
      let foundPending = hasPending(entries);

      this.setState({
        balance,
        hasPending: foundPending
      });
    } else {
      // No entries were found, reset to initial state
      this.setState(this.getInitialState());
    }
  }

  render() {
    let pending = this.state.hasPending ? <Image style={styles.pending} source={Images.PENDING}/> : null;

    return (
      <View style={styles.container}>
        <TouchableOpacity style={styles.rowContainer} onPress={this.recordTapped.bind(this)}>
            <Text style={styles.leftTab}>{this.recordInitial()}</Text>
            <Text style={styles.username}>{this.recordTitle()}</Text>
            <Text style={styles.sign}>{this.recordSign()}</Text>
            <Text style={styles.balance}>{formatBalance(this.state.balance)}</Text>
        </TouchableOpacity>
        {pending}
      </View>
    );
  }

  recordTapped() {
    Analytics.logEvent(Analytics.EVENT_VIEW_ACCOUNT, {
      hasPending: this.state.hasPending,
      isNearby: this.props.isUserNearby,
      balance: this.state.balance
    });

    this.props.navigator.push({
      screen: Screens.SCREEN_ACCOUNT,
      title: this.recordTitle(),
      passProps: {
        otherUser: this.props.user
      },
      navigatorStyle: {
        navBarHidden: true,
        statusBarColor: Styles.Colors.SECONDARY
      },
      animationType: 'slide-up'
    });
  }

  recordInitial() {
    return this.props.user.name ? this.props.user.name.toUpperCase().charAt(0) : null;
  }

  recordTitle() {
    return this.props.user.name;
  }

  recordSign() {
    if (this.state.balance === 0) {
      return '';
    } else {
      return this.state.balance > 0 ? '+' : '-';
    }
  }
}

const styles = StyleSheet.create({
  container: {
    marginHorizontal: '3%'
  },
  rowContainer: {
    flexDirection: 'row',
    ...Styles.Shapes.ROUNDED_RECT_LARGE_NO_BORDER,
    overflow: 'hidden',
    alignItems: 'center',
    marginBottom: Styles.Shapes.ROW_SPACE,
    elevation: 1 // Android Only
  },
  leftTab: {
    width: '10%',
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 15,
    color: Styles.Colors.WHITE,
    backgroundColor: Styles.Colors.SECONDARY,
    textAlign: 'center',
    paddingVertical: Styles.Shapes.RECT_MARGIN_SIZE
  },
  username: {
    width: '60%',
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 15,
    paddingLeft: '5%'
  },
  sign: {
    width: '5%',
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 15
  },
  balance: {
    width: '20%',
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 15,
    textAlign: 'right'
  },
  pending: {
    position: 'absolute',
    right: -5
  }
});

const mapStateToProps = function(state) {
  return {
    userID: state.user.user.id,
    allEntries: state.records.allEntries
  };
};

export default connect(mapStateToProps)(RecordRow);
