/**
 * SideMenu
 * @flow
 */
import React, { Component } from 'react';
import {
  View,
  Text,
  FlatList,
  TouchableOpacity,
  StyleSheet,
  Share
} from 'react-native';
import { connect } from 'react-redux';
import Firebase from '../managers/Firebase';
import * as Analytics from '../managers/Analytics';
import * as Screens from '../screens/screens';
import * as Styles from './styles/Styles';
import * as Strings from './strings/Strings';

class SideMenu extends Component {

  constructor(props) {
    super(props);

    this.menuItems = [
      {key: 'Invite a Friend', function: this.goToInvite.bind(this)},
      {key: 'Send Feedback', function: this.goToSendFeedback.bind(this)},
      {key: 'Logout', function: this.logoutTapped.bind(this)}
    ];
  }

  renderItem({item}) {
    return (
      <TouchableOpacity onPress={item.function}>
        <Text style={styles.menuLabel}>{item.key}</Text>
      </TouchableOpacity>
    );
  }

  render() {
    return (
      <View style={styles.sideContainer}>
        <Text style={styles.username}>{this.props.user ? this.props.user.name : null}</Text>
        <FlatList
          data={this.menuItems}
          renderItem={this.renderItem.bind(this)}
        />
        <Text style={styles.version}>{Strings.VERSION_STRING}</Text>
      </View>
    );
  }

  goToInvite() {
    Analytics.logEvent(Analytics.EVENT_SHARE_OPENED);

    this.props.navigator.push({
      screen: Screens.SCREEN_INVITE,
      navigatorStyle: {
        navBarHidden: true,
        statusBarColor: Styles.Colors.SECONDARY
      },
      animationType: 'fade'
    });
    this.hideMenu();
  }

  goToSendFeedback() {
    Analytics.logEvent(Analytics.EVENT_VIEW_SEND_FEEDBACK);

    this.props.navigator.push({
      screen: Screens.SCREEN_FEEDBACK,
      navigatorStyle: {
        navBarHidden: true,
        statusBarColor: Styles.Colors.SECONDARY
      },
      animationType: 'fade'
    });
    this.hideMenu();
  }

  logoutTapped() {
    this.hideMenu();
    Firebase.getInstance().signOut();
  }

  hideMenu() {
    this.props.navigator.toggleDrawer({
      side: 'right',
      animated: true,
      to: 'close'
    });
  }
}

const styles = StyleSheet.create({
  sideContainer: {
    flex: 1,
    backgroundColor: 'white'
  },
  username: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 18,
    color: Styles.Colors.SECONDARY,
    marginLeft: '10%',
    marginVertical: '20%'
  },
  menuLabel: {
    fontFamily: Styles.Fonts.REGULAR,
    fontSize: 16,
    color: Styles.Colors.GREY,
    marginVertical: Styles.Shapes.RECT_MARGIN_SIZE,
    marginLeft: '10%'
  },
  version: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 14,
    color: Styles.Colors.LIGHT_GREY,
    alignSelf: 'center',
    marginBottom: '10%'
  }
});

const mapStateToProps = function(state) {
  return {
    user: state.user.user
  };
};

export default connect(mapStateToProps)(SideMenu);
