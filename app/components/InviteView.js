/**
 * InviteView
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  TextInput,
  Share,
  Image
} from 'react-native';
import { connect } from 'react-redux';
import Images from './images/Images';
import * as Analytics from '../managers/Analytics';
import * as Styles from './styles/Styles';
import * as Strings from './strings/Strings';

class InviteView extends Component {

  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.title}>Invite a Friend</Text>
        <Text style={styles.instructions}>Share the following link with others.</Text>
        <TouchableOpacity style={styles.shareArea} onPress={this.sharePinch.bind(this)}>
          <Image source={Images.SHARE}/>
          <TextInput editable={false} style={styles.link} multiline={true}>{this.shareURL()}</TextInput>
        </TouchableOpacity>
        <Text style={styles.details}>Friends that are already Pinch users will be added to your 'Contacts' section once they open the app using the link.</Text>
      </View>
    );
  }

  sharePinch() {
    let content = {
      title: this.props.user.name + ' would like to invite you to use Pinch.',
      message: this.shareURL()
    };
    let options = {
      dialogTitle: 'Share Pinch Invite'
    };

    Analytics.logEvent(Analytics.EVENT_SHARE_TAPPED);
    Share.share(content, options);
  }

  shareURL() {
    return Strings.SHARE_URL_BASE + this.props.user.id;
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '10%',
    backgroundColor: 'white',
    alignItems: 'center'
  },
  title: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 20,
    color: Styles.Colors.SECONDARY,
    marginBottom: '5%'
  },
  shareArea: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  link: {
    textAlign: 'left'
  },
  instructions: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 16,
    color: Styles.Colors.GREY
  },
  details: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 14,
    color: Styles.Colors.LIGHT_GREY
  }
});

const mapStateToProps = function(state) {
  return {
    user: state.user.user
  };
};

export default connect(mapStateToProps)(InviteView);
