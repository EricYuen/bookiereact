/**
 * RecordsView
 * @flow
 */

import React, { Component } from 'react';
import {
  ActivityIndicator,
  View,
  SectionList,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity
} from 'react-native';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { calculateBalance, sortUsersByName, sortUsersByDistance, isNearby, formatBalance } from '../managers/recordsHelper';
import { getValuesFromDictionary } from '../managers/keyGenerators';
import * as Analytics from '../managers/Analytics';
import UserLoader from '../managers/UserLoader';
import * as Styles from './styles/Styles';
import Images from './images/Images';
import RecordRow from './RecordRow';
import * as userActions from '../actions/userActions';
import * as recordsActions from '../actions/recordsActions';

const SECTION_NEARBY = 'Nearby';
const SECTION_FRIENDS = 'Contacts';

class RecordsView extends Component {

  state: {
    data: Array,
    balance: number,
    totalLent: number,
    totalBorrowed: number,
    refreshing: boolean
  }

  constructor(props) {
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      data: [
        {title: SECTION_NEARBY, data: []},
        {title: SECTION_FRIENDS, data: []}
      ],
      balance: 0,
      totalLent: 0,
      totalBorrowed: 0,
      refreshing: false
    };
  }

  componentWillReceiveProps(nextProps) {
    // If user logged out
    if (this.props.user && !nextProps.user) {
      this.setState(this.getInitialState());
      return;
    }

    // If the list of records has changed
    let recordChanged = JSON.stringify(this.props.records) !== JSON.stringify(nextProps.records);
    let locationsChanged = JSON.stringify(this.props.allLocations) !== JSON.stringify(nextProps.allLocations);
    let entriesChanged = JSON.stringify(this.props.allEntries) !== JSON.stringify(nextProps.allEntries);

    if (recordChanged || locationsChanged || entriesChanged) {
      let nearbyUsers = [];
      let friends = [];
      for (let i = 0; i < nextProps.records.length; i++) {
        let record = nextProps.records[i];
        if (isNearby(nextProps.allLocations[record.id])) {
          nearbyUsers.push(record);
        } else {
          friends.push(record);
        }
      }

      // Sort nearby by distance
      sortUsersByDistance(nearbyUsers, nextProps.allLocations);
      // Sort friends by name
      sortUsersByName(friends);

      // Get total balances
      let balance = 0;
      let totalLent = 0;
      let totalBorrowed = 0;
      for (let i = 0; i < this.props.records.length; i++) {
        let recordID = this.props.records[i].id;
        if (nextProps.allEntries[recordID]) {
          let entryBal = calculateBalance(nextProps.user.id, nextProps.allEntries[recordID]);
          balance += entryBal.balance;
          if (entryBal.balance > 0) {
            totalLent += entryBal.balance;
          } else if (entryBal.balance < 0) {
            totalBorrowed += entryBal.balance;
          }
        }
      }

      // Refresh screen state
      this.setState({
        data: [
          {title: SECTION_NEARBY, data: nearbyUsers},
          {title: SECTION_FRIENDS, data: friends}
        ],
        balance,
        totalLent,
        totalBorrowed
      });
    }
  }

  renderSectionHeader({section: {title}}) {
    let lineColor;
    let fontColor;
    if (title === SECTION_NEARBY) {
      lineColor = { backgroundColor: Styles.Colors.SECONDARY };
      fontColor = { color: Styles.Colors.SECONDARY };
    } else if (title === SECTION_FRIENDS) {
      lineColor = { backgroundColor: Styles.Colors.PRIMARY };
      fontColor = { color: Styles.Colors.PRIMARY };
    }
    return (
      <View style={styles.sectionHeader}>
        <View style={[styles.sectionLine, lineColor]}/>
        <Text style={[styles.sectionLabel, fontColor]}>{title}</Text>
        <View style={[styles.sectionLine, lineColor]}/>
      </View>
    );
  }

  renderItem({item, index, section}) {
    let user = item;
    let isUserNearby = section.title === SECTION_NEARBY;
    return <RecordRow user={user} isUserNearby={isUserNearby} navigator={this.props.navigator}/>;
  }

  renderEmptyMessage({section: {title, data}}) {
    if (this.state.refreshing) {
      return <ActivityIndicator style={{flex: 1}}/>;
    } else if (data.length !== 0) {
      return null;
    }

    let emptyImage;
    let emptyTitle;
    let emptyDescription;
    if (title === SECTION_NEARBY) {
      emptyImage = Images.NO_LOCATION;
      emptyTitle = 'No users nearby';
      if (this.props.locationError) {
        if (this.props.locationError.denied) {
          emptyDescription = 'Please allow location permissions and try again.';
        } else {
          emptyDescription = 'Please enable location services and try again.';
        }
      } else {
        emptyDescription = null;
      }
    } else if (title === SECTION_FRIENDS) {
      emptyImage = Images.CONTACTS;
      emptyTitle = 'No contacts found';
      emptyDescription = 'Contacts will be added once the first IOU is created.';
    }

    return (
      <View style={styles.emptyContainer}>
        <Image style={styles.emptyImage} source={emptyImage}/>
        {emptyTitle ? <Text style={styles.emptyText}>{emptyTitle}</Text> : null}
        {emptyDescription ? <Text style={styles.emptyText}>{emptyDescription}</Text> : null}
      </View>
    );
  }

  render() {
    if (!this.props.user) {
      return <ActivityIndicator style={{flex: 1}}/>;
    }

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Image style={styles.logo} source={Images.LOGO}/>
          <TouchableOpacity onPress={this.openSideMenu.bind(this)}>
            <Image source={Images.MENU_ICON}/>
          </TouchableOpacity>
        </View>
        <SectionList
          sections={this.state.data}
          renderSectionHeader={this.renderSectionHeader.bind(this)}
          renderItem={this.renderItem.bind(this)}
          renderSectionFooter={this.renderEmptyMessage.bind(this)}
          keyExtractor={(item, index) => item + index}
          refreshing={this.state.refreshing}
          onRefresh={this.onRefresh.bind(this)}
        />
        <Text style={styles.balanceText}>{formatBalance(this.state.balance, true)}</Text>
        <View style={styles.totalsContainer}>
          <Text style={styles.totalsText}>+ {formatBalance(this.state.totalLent, false)}</Text>
          <Text style={styles.totalsText}>- {formatBalance(this.state.totalBorrowed, false)}</Text>
        </View>
      </View>
    );
  }

  openSideMenu() {
    Analytics.logEvent(Analytics.EVENT_OPEN_SIDE_MENU);

    this.props.navigator.toggleDrawer({
      side: 'right',
      animated: true,
      to: 'open'
    });
  }

  onRefresh() {
    this.setState({refreshing: true});
    UserLoader.refreshUserList(this.props.user.id).finally(() => {
      this.setState({refreshing: false});
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    marginVertical: '3%'
  },
  header: {
    flexDirection: 'row',
    marginTop: '3%',
    marginHorizontal: '8%',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  logo: {
    resizeMode: 'contain'
  },
  sectionHeader: {
    flexDirection: 'row',
    alignItems: 'center',
    marginHorizontal: '3%'
  },
  sectionLine: {
    flex: 1,
    height: Styles.Shapes.LINE_THICKNESS
  },
  sectionLabel: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 11,
    margin: Styles.Shapes.MARGIN_MEDIUM
  },
  balanceText: {
    textAlign: 'center',
    fontFamily: Styles.Fonts.SEMI_BOLD,
    fontSize: 42,
    color: Styles.Colors.PRIMARY
  },
  totalsContainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginHorizontal: '20%'
  },
  totalsText: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 17,
    color: Styles.Colors.GREY
  },
  emptyContainer: {
    flex: 1,
    alignItems: 'center',
    margin: '3%'
  },
  emptyImage: {
    marginBottom: '3%'
  },
  emptyText: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 13,
    color: Styles.Colors.LIGHT_GREY
  }
});

const mapStateToProps = function(state) {
  return {
    user: state.user.user,
    records:  getValuesFromDictionary(state.records.records),
    allLocations:  state.records.allLocations,
    allEntries:  state.records.allEntries,
    locationError: state.location.locationError
  };
};

const mapDispatchToProps = function(dispatch: Function) {
  return {
    userActions: bindActionCreators(userActions, dispatch),
    recordsActions: bindActionCreators(recordsActions, dispatch)
  };
};

export default connect(mapStateToProps, mapDispatchToProps)(RecordsView);
