/**
 * SendFeedback
 * @flow
 */
import React, { Component } from 'react';
import {
  View,
  TextInput,
  StyleSheet,
  TouchableOpacity,
  Text
} from 'react-native';
import { connect } from 'react-redux';
import * as Styles from './styles/Styles';
import * as Strings from './strings/Strings';
import Firebase from '../managers/Firebase';
import * as Analytics from '../managers/Analytics';

class SendFeedbackView extends Component {

  state: {
    feedback: string
  }

  constructor(props) {
    super(props);

    this.state = {
      feedback: null
    };
  }

  render() {
    let disableSend = !this.state.feedback;
    let buttonStyle = [styles.sendButton, (disableSend ? styles.sendDisable : null)];

    return (
      <View style={styles.container}>
        <TextInput
          style={styles.feedbackBox}
          value={this.state.feedback}
          placeholder={'Write feedback'}
          placeholderTextColor={Styles.Colors.LIGHT_GREY}
          multiline={true}
          numberOfLines={5}
          onChangeText={(text) => {
            this.setState({feedback: text});
          }}
        />
        <TouchableOpacity disabled={disableSend} style={buttonStyle} onPress={this.sendFeedback.bind(this)}>
          <Text style={styles.sendLabel}>Send</Text>
        </TouchableOpacity>
      </View>
    );
  }

  sendFeedback(){
    Analytics.logEvent(Analytics.EVENT_SEND_FEEDBACK, {length: this.state.feedback.length, version: Strings.VERSION_STRING});

    Firebase.getInstance().sendFeedback(this.props.user.id, this.state.feedback, Strings.VERSION_STRING, new Date());
    this.props.navigator.pop();
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: '10%',
    backgroundColor: 'white',
    alignItems: 'center'
  },
  feedbackBox: {
    width: '100%',
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    borderColor: Styles.Colors.LIGHT_GREY,
    color: Styles.Colors.GREY,
    fontFamily: Styles.Fonts.REGULAR,
    textAlignVertical: 'top', // Android only
    fontSize: 13,
    marginBottom: '5%'
  },
  sendButton: {
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    borderColor: Styles.Colors.SECONDARY,
    backgroundColor: Styles.Colors.SECONDARY,
    width: '100%',
    alignItems: 'center',
    paddingVertical: 3
  },
  sendLabel: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 13,
    color: Styles.Colors.WHITE
  },
  sendDisable:{
    opacity: 0.45
  }
});

const mapStateToProps = function(state) {
  return {
    user: state.user.user
  };
};

export default connect(mapStateToProps)(SendFeedbackView);
