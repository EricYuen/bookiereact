/**
 * Images
 * @flow
 */

export default {
  LOGO: require('../../../assets/images/logo.png'),
  LOGO_KO: require('../../../assets/images/logo_ko.png'),
  MENU_ICON: require('../../../assets/images/menu_icon.png'),
  SHARE: require('../../../assets/images/share.png'),
  PENDING: require('../../../assets/images/pending.png'),
  CLOSE: require('../../../assets/images/close.png'),
  NO_LOCATION: require('../../../assets/images/no_location.png'),
  CONTACTS: require('../../../assets/images/contacts.png')
};
