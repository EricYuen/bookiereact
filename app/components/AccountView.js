/**
 * AccountView
 * @flow
 */

import React, { Component } from 'react';
import {
  View,
  TextInput,
  Text,
  StyleSheet,
  Image,
  TouchableOpacity,
  FlatList
} from 'react-native';
import { connect } from 'react-redux';
import moment from 'moment';

import Firebase from '../managers/Firebase';
import * as Analytics from '../managers/Analytics';
import { accountPairingKey } from '../managers/keyGenerators';
import { calculateBalance, hasPending, formatBalance, formatAmountInput} from '../managers/recordsHelper';
import Entry from '../models/Entry';
import Images from './images/Images';
import * as Styles from './styles/Styles';

const SELECTED_NONE = 0;
const SELECTED_LEND = 1;
const SELECTED_BORROW = 2;

class AccountView extends Component {
  props: {
    userID: string, // Current User ID
    otherUser: string // Other User ID
  }

  state: {
    balance: number,
    selected: number,
    amount: number,
    amountText: string,
    note: string,
    isWaitingApproval: boolean,
    dataSource: Array
  }

  constructor(props){
    super(props);
    this.state = this.getInitialState();
  }

  getInitialState() {
    return {
      balance: 0,
      selected: SELECTED_NONE,
      amount: 0,
      amountText: null,
      note: null,
      isWaitingApproval: false,
      dataSource: []
    };
  }

  componentWillMount() {
    this.refreshRecordEntries(this.props.allEntries[this.props.otherUser.id]);
  }

  componentWillReceiveProps(nextProps) {
    // Refresh the entries if there is change
    let oldEntries = this.props.allEntries[this.props.otherUser.id];
    let newEntries = nextProps.allEntries[this.props.otherUser.id];
    if (JSON.stringify(oldEntries) !== JSON.stringify(newEntries)) {
      this.refreshRecordEntries(newEntries);
    }
  }

  refreshRecordEntries(entries) {
    if (entries && !entries.empty) {
      // Calculate new balance
      let { balance } = calculateBalance(this.props.userID, entries);
      let selected = SELECTED_NONE;
      let amount = 0;
      let note = null;

      // Initialize the amount and selected state based on the first entry (if waiting for approval)
      let isWaitingApproval = hasPending(entries);
      if (isWaitingApproval) {
        let firstEntry = entries[0];
        amount = firstEntry.amount;
        if (firstEntry.lender === this.props.userID){
          selected = SELECTED_LEND;
        } else if (firstEntry.borrower === this.props.userID){
          selected = SELECTED_BORROW;
        }
        note = firstEntry.note;
      }

      this.setState({
        balance,
        selected,
        amount,
        note,
        isWaitingApproval,
        dataSource: entries
      });
    } else {
      // No entries were found, reset to initial state
      this.setState(this.getInitialState());
    }
  }

  renderHeader() {
    // Display the appropriate approval relative to the user
    let pendingText = ' ';
    if (this.state.isWaitingApproval) {
      // Assume the pending entry is the first in the array
      let firstEntry = this.state.dataSource[0];
      pendingText = firstEntry.needsApprovalFromUser(this.props.userID) ? 'PLEASE CONFIRM' : 'PENDING';
    }

    return (
      <View style={styles.header}>
        <View style={styles.pendingRow}>
          {this.state.isWaitingApproval ? <Image style={styles.pendingIcon} source={Images.PENDING}/> : null}
          <Text style={styles.pendingText}>{pendingText}</Text>
        </View>
        <TouchableOpacity style={styles.close} onPress={this.closeTapped.bind(this)}>
          <Image source={Images.CLOSE}/>
        </TouchableOpacity>
      </View>
    );
  }

  renderAccountInfo() {
    return (
      <View style={styles.accountInfo}>
        <Text style={styles.name}>{this.props.otherUser.name}</Text>
        <Text style={styles.balance}>{formatBalance(this.state.balance, true)}</Text>
      </View>
    );
  }

  renderAmountInput() {
    return (
      <TextInput
        style={styles.amountInput}
        ref={(component) => {this.amountInput = component;}}
        placeholder="AMOUNT"
        value={this.state.amountText}
        onChangeText={(amountText) => {
          amountText = formatAmountInput(amountText);
          // Remove the $ sign so it can be parsed as a float
          let amount = amountText.substring(1);
          amount = isNaN(parseFloat(amount)) ? 0 : parseFloat(amount);
          this.setState({
            amountText,
            amount
          });
        }}
        defaultValue={this.state.isWaitingApproval ? '$' + this.state.amount.toFixed(2) : null}
        editable={!this.state.isWaitingApproval}
        keyboardType="numeric"
        underlineColorAndroid="transparent"
      />
    );
  }

  renderNoteInput() {
    return (
      <TextInput
        style={styles.noteInput}
        placeholder="Note (optional)"
        placeholderColor={Styles.Colors.LIGHT_GREY}
        value={this.state.note}
        onChangeText={(note) => this.setState({note})}
        editable={!this.state.isWaitingApproval}
        underlineColorAndroid={Styles.Colors.NEAR_WHITE}
      />
    );
  }

  renderOptionButtons() {
    // Flip between styles depending on toggle
    let lendButtonStyle = [styles.lendButton, styles.unselectedButton];
    let lendButtonLabelStyles = [styles.labelText, styles.unselectedButtonLabel];
    let borrowButtonStyle = [styles.borrowButton, styles.unselectedButton];
    let borrowButtonLabelStyle = [styles.labelText, styles.unselectedButtonLabel];
    if (this.state.selected === SELECTED_LEND) {
      lendButtonStyle = [styles.lendButton, styles.selectedButton];
      lendButtonLabelStyles = [styles.labelText, styles.selectedButtonLabel];
    } else if (this.state.selected === SELECTED_BORROW) {
      borrowButtonStyle = [styles.borrowButton, styles.selectedButton];
      borrowButtonLabelStyle = [styles.labelText, styles.selectedButtonLabel];
    }

    return (
      <View style={styles.optionButtons}>
        <TouchableOpacity style={lendButtonStyle} disabled={this.state.isWaitingApproval} onPress={this.selectAmountDirection.bind(this, SELECTED_LEND)}>
            <View style={styles.labelView}>
              <Text style={[lendButtonLabelStyles, styles.sign]}>+ </Text>
              <Text style={lendButtonLabelStyles}>Lend</Text>
            </View>
        </TouchableOpacity>
        <TouchableOpacity style={borrowButtonStyle} disabled={this.state.isWaitingApproval} onPress={this.selectAmountDirection.bind(this, SELECTED_BORROW)}>
            <View style={styles.labelView}>
              <Text style={[borrowButtonLabelStyle, styles.sign]}>- </Text>
              <Text style={borrowButtonLabelStyle}>Borrow</Text>
            </View>
        </TouchableOpacity>
      </View>
    );
  }

  renderConfirmButton() {
    let readyToConfirm = this.state.amount !== 0 && this.state.selected !== SELECTED_NONE;
    let buttonStyle = [styles.confirmButton, (readyToConfirm ? null : styles.confirmDisable)];
    return (
      <View style={styles.pendingButtons}>
        <TouchableOpacity key="confirm" disabled={!readyToConfirm} style={buttonStyle} onPress={this.saveEntry.bind(this)}>
          <Text style={styles.confirmLabel}>Confirm</Text>
        </TouchableOpacity>
      </View>
    );
  }

  renderPendingButtons() {
    // Assume the pending entry is the first in the array
    let buttons = [];
    let firstEntry = this.state.dataSource[0];
    if (firstEntry.needsApprovalFromUser(this.props.userID)) {
      // Button to approve record
      buttons.push(
        <TouchableOpacity key="confirm" style={styles.confirmButton} onPress={this.approveEntry.bind(this, firstEntry)}>
          <Text style={styles.confirmLabel}>Confirm</Text>
        </TouchableOpacity>
      );
    }
    // Button to cancel the record
    buttons.push(
      <TouchableOpacity key="cancel" style={styles.cancelButton} onPress={this.declineEntry.bind(this, firstEntry)}>
        <Text style={[styles.labelText, styles.cancelLabel]}>Cancel</Text>
      </TouchableOpacity>
    );

    return (
      <View style={styles.pendingButtons}>
        {buttons}
      </View>
    );
  }

  renderHistory() {
    return (
      <View style={styles.historyArea}>
        <View style={styles.historyHeader}>
          <View style={styles.historyLine}/>
          <Text style={styles.historyHeaderLabel}>History</Text>
          <View style={styles.historyLine}/>
        </View>
        <FlatList
          data={this.state.dataSource}
          renderItem={this.renderHistoryRow.bind(this)}
          keyExtractor={(item, index) => {return item.id;}}
          showsVerticalScrollIndicator={false}
        />
      </View>
    );
  }

  renderHistoryRow({item}) {
    // Do not render items that were cancelled or not approved by both users ie. Not included in the balance
    if (item.cancelled || !item.borrowerApproved || !item.lenderApproved) {
      return <View/>;
    }

    let sign = '+';
    if (this.props.userID === item.borrower) {
      sign = '-';
    }

    return (
      <View style={styles.historyRow}>
        <View style={styles.historyEntryArea}>
          <Text style={styles.historyNoteText}>{item.note ? item.note : '-'}</Text>
          <Text style={styles.historyTimestampText}>{moment(item.timestamp).format('ddd MMM D, h:mma')}</Text>
        </View>
        <Text style={styles.historyAmountText}>{sign}{formatBalance(item.amount)}</Text>
      </View>
    );
  }

  render() {
    return (
      <View style={styles.container}>
        {this.renderHeader()}
        {this.renderAccountInfo()}
        {this.renderAmountInput()}
        {this.renderNoteInput()}
        {this.renderOptionButtons()}
        {this.state.isWaitingApproval ? this.renderPendingButtons() : this.renderConfirmButton()}
        {this.renderHistory()}
      </View>
    );
  }

  selectAmountDirection(select) {
    Analytics.logEvent(select === SELECTED_LEND ? Analytics.EVENT_LEND_PRESSED : Analytics.EVENT_BORROW_PRESSED, {amount: this.state.amount});

    this.setState({selected: select});
  }

  saveEntry() {
    Analytics.logEvent(this.state.selected === SELECTED_LEND ? Analytics.EVENT_LEND_CREATED : Analytics.EVENT_BORROW_CREATED, {amount: this.state.amount});

    let pairingKey = accountPairingKey(this.props.userID, this.props.otherUser.id);
    let lender = this.state.selected === SELECTED_LEND ? this.props.userID : this.props.otherUser.id;
    let borrower = this.state.selected === SELECTED_BORROW ? this.props.userID : this.props.otherUser.id;

    // Save the record to Firebase
    let entry = new Entry();
    entry.init(lender, borrower, this.state.amount, new Date());
    if (this.state.note) {
      entry.note = this.state.note;
    }
    // Creator of the entry automatically approves it
    entry.approveByUser(this.props.userID);
    Firebase.getInstance().addEntryToRecord(pairingKey, entry);
  }

  approveEntry(entry) {
    Analytics.logEvent(this.state.selected === SELECTED_LEND ? Analytics.EVENT_LEND_CONFIRMED : Analytics.EVENT_BORROW_CONFIRMED, {amount: this.state.amount});

    let pairingKey = accountPairingKey(this.props.userID, this.props.otherUser.id);
    Firebase.getInstance().approveEntryByUser(pairingKey, entry, this.props.userID);
  }

  declineEntry(entry) {
    // Log the event based on whether it's the creator or (else) receiver
    if (entry.needsApprovalFromUser(this.props.userID)) {
      Analytics.logEvent(this.state.selected === SELECTED_LEND ? Analytics.EVENT_LEND_REJECTED : Analytics.EVENT_BORROW_REJECTED, {amount: this.state.amount});
    } else {
      Analytics.logEvent(this.state.selected === SELECTED_LEND ? Analytics.EVENT_LEND_CANCELLED : Analytics.EVENT_BORROW_CANCELLED, {amount: this.state.amount});
    }

    let pairingKey = accountPairingKey(this.props.userID, this.props.otherUser.id);
    Firebase.getInstance().cancelEntry(pairingKey, entry);
  }

  resetValues(){
    this.setState({
      amount: 0
    });
  }

  closeTapped() {
    Analytics.logEvent(Analytics.EVENT_CLOSE_ACCOUNT);

    this.props.navigator.pop({
      animationType: 'slide-down'
    });
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    paddingTop: '5%',
    paddingHorizontal: '10%'
  },
  header: {
    width: '100%',
    alignSelf: 'center'
  },
  pendingRow: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center'
  },
  pendingIcon: {
    marginRight: Styles.Shapes.MARGIN_MEDIUM
  },
  pendingText: {
    fontSize: 13,
    textAlign: 'center',
    color: Styles.Colors.GREY
  },
  close: {
    position: 'absolute',
    top: 0,
    right: '-5%'
  },
  accountInfo: {
    flexDirection: 'row',
    borderBottomWidth: Styles.Shapes.LINE_THICKNESS,
    borderColor: Styles.Colors.LIGHT_GREY,
    marginTop: '7%',
    marginBottom: '5%',
    justifyContent: 'space-between',
    alignItems: 'baseline'
  },
  name: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 20,
    color: Styles.Colors.SECONDARY
  },
  balance: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 15
  },
  amountInput: {
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    borderColor: Styles.Colors.LIGHT_GREY,
    textAlign: 'center',
    color: Styles.Colors.GREY,
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 20
  },
  noteInput: {
    color: Styles.Colors.GREY,
    fontFamily: Styles.Fonts.REGULAR,
    marginBottom: '5%'
  },
  optionButtons: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginBottom: Styles.Shapes.MARGIN_MEDIUM
  },
  labelView: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 2
  },
  labelText: {
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 13
  },
  lendButton: {
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    flex: 1,
    marginRight: '5%'
  },
  borrowButton: {
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    flex: 1
  },
  unselectedButton: {
    borderColor: Styles.Colors.LIGHT_GREY,
    backgroundColor: Styles.Colors.LIGHT_GREY
  },
  unselectedButtonLabel: {
    color: Styles.Colors.GREY,
    textAlign: 'center'
  },
  selectedButton: {
    borderColor: Styles.Colors.PRIMARY,
    backgroundColor: Styles.Colors.PRIMARY
  },
  selectedButtonLabel: {
    color: Styles.Colors.WHITE,
    textAlign: 'center'
  },
  sign: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 16
  },
  pendingButtons: {
    alignItems: 'center'
  },
  confirmButton: {
    ...Styles.Shapes.ROUNDED_RECT_SMALL,
    borderColor: Styles.Colors.SECONDARY,
    backgroundColor: Styles.Colors.SECONDARY,
    width: '47.5%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 3,
    marginVertical: Styles.Shapes.MARGIN_MEDIUM
  },
  confirmLabel: {
    fontFamily: Styles.Fonts.BOLD,
    fontSize: 13,
    color: Styles.Colors.WHITE
  },
  confirmDisable:{
    opacity: 0.45
  },
  cancelButton: {
    width: '47.5%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 3,
    marginVertical: Styles.Shapes.MARGIN_MEDIUM
  },
  cancelLabel: {
    color: Styles.Colors.RED
  },
  historyArea: {
    flex: 1,
    marginHorizontal: '5%',
    marginTop: '7%'
  },
  historyHeader: {
    flexDirection: 'row',
    alignItems: 'center'
  },
  historyLine: {
    flex: 1,
    height: Styles.Shapes.LINE_THICKNESS,
    backgroundColor: Styles.Colors.LIGHT_GREY
  },
  historyHeaderLabel :{
    fontFamily: Styles.Fonts.LIGHT,
    fontSize: 11,
    margin: Styles.Shapes.MARGIN_MEDIUM,
    color: Styles.Colors.LIGHT_GREY
  },
  historyRow: {
    flexDirection: 'row',
    alignItems: 'center',
    borderBottomWidth: 1,
    borderColor: Styles.Colors.NEAR_WHITE,
    paddingVertical: Styles.Shapes.MARGIN_MEDIUM
  },
  historyEntryArea: {
    width: '75%'
  },
  historyNoteText: {
    fontFamily: Styles.Fonts.REGULAR,
    color: Styles.Colors.GREY
  },
  historyAmountText: {
    width: '25%',
    fontFamily: Styles.Fonts.REGULAR,
    color: Styles.Colors.GREY,
    textAlign: 'right'
  },
  historyTimestampText: {
    fontFamily: Styles.Fonts.REGULAR,
    fontSize: 10,
    color: Styles.Colors.LIGHT_GREY
  }
});

const mapStateToProps = function(state) {
  return {
    userID: state.user.user.id,
    allEntries: state.records.allEntries
  };
};

export default connect(mapStateToProps)(AccountView);
