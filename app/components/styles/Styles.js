/**
 * Styles
 * @flow
 */

import { PixelRatio } from 'react-native';

export const Fonts = {
  REGULAR: 'Nunito-Regular',
  LIGHT: 'Nunito-Light',
  BOLD: 'Nunito-Bold',
  SEMI_BOLD: 'Nunito-SemiBold'
};

export const Colors = {
  PRIMARY: '#F37021',
  SECONDARY: '#00ACBF',
  GREY: '#6D6E71',
  LIGHT_GREY: '#BDBDBE',
  NEAR_WHITE: '#E8E8E8',
  WHITE: 'white',
  TRANSLUCENT_WHITE: '#FFFFFFF6',
  TRANSLUCENT_BLACK: '#00000064',
  RED: 'red'
};

// Constant that will be referred to in Shapes
const LINE_THICKNESS = 1;
const BORDER_RADIUS_SMALL = PixelRatio.getPixelSizeForLayoutSize(1);
const BORDER_RADIUS_LARGE = PixelRatio.getPixelSizeForLayoutSize(3);

export const Shapes = {
  LINE_THICKNESS: LINE_THICKNESS,
  BORDER_RADIUS_SMALL: BORDER_RADIUS_SMALL,
  BORDER_RADIUS_LARGE: BORDER_RADIUS_LARGE,
  ROW_SPACE: PixelRatio.getPixelSizeForLayoutSize(2),
  MARGIN_SMALL: PixelRatio.getPixelSizeForLayoutSize(1),
  MARGIN_MEDIUM: PixelRatio.getPixelSizeForLayoutSize(2),
  MARGIN_LARGE: PixelRatio.getPixelSizeForLayoutSize(6),
  RECT_MARGIN_SIZE: PixelRatio.getPixelSizeForLayoutSize(3),
  SIDE_MENU_WIDTH: PixelRatio.getPixelSizeForLayoutSize(150),

  ROUNDED_RECT_SMALL_NO_BORDER: {
    borderRadius: BORDER_RADIUS_SMALL
  },

  ROUNDED_RECT_LARGE_NO_BORDER: {
    borderRadius: BORDER_RADIUS_LARGE
  },

  ROUNDED_RECT_SMALL: {
    borderWidth: LINE_THICKNESS,
    borderStyle: 'solid',
    borderRadius: BORDER_RADIUS_SMALL
  },

  ROUNDED_RECT_LARGE: {
    borderWidth: LINE_THICKNESS,
    borderStyle: 'solid',
    borderRadius: BORDER_RADIUS_LARGE
  }
};

export const Components = {
  TEXT_INPUT: {
    ...Shapes.ROUNDED_RECT_LARGE_NO_BORDER,
    textAlign: 'center',
    color: Colors.GREY,
    backgroundColor: Colors.WHITE,
    elevation: 1
  }
};
