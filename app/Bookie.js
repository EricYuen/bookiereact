/**
 * Bookie
 * @flow
 */

import { Navigation } from 'react-native-navigation';

// Imports for Redux
import { createStore, combineReducers } from 'redux';
import { Provider } from 'react-redux';
import * as reducers from './reducers';

// Imports for Navigator
import * as Screens from './screens/screens';
import * as Styles from './components/styles/Styles';

import UserLoader from './managers/UserLoader';
import * as PinchLocator from './managers/PinchLocator';
import DeepLinking from './managers/DeepLinking';

export function launch() {
  // Redux store setup
  const reducer = combineReducers(reducers);
  const store = createStore(reducer);

  UserLoader.init(store);
  PinchLocator.init(store);
  DeepLinking.init(store);

  // Initialize Navigation
  Screens.registerScreens(store, Provider);
  Navigation.startSingleScreenApp({
    screen: {
      screen: Screens.SCREEN_MAIN,
      navigatorStyle: {
        navBarHidden: true,
        statusBarColor: Styles.Colors.SECONDARY
      }
    },
    drawer: {
      right: {
        screen: Screens.SCREEN_SIDE_MENU,
        fixedWidth: Styles.Shapes.SIDE_MENU_WIDTH
      }
    }
  });
}
