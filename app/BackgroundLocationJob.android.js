/**
 * BackgroundLocationJob (Android)
 * @flow
 */
import PushNotification from 'react-native-push-notification';

import GeofireWrapper from './managers/GeofireWrapper';
import PersistentStore from './managers/PersistentStore';
import Locator from './managers/Locator';
import Firebase from './managers/Firebase';
import { accountPairingKey } from './managers/keyGenerators';
import { calculateBalance } from './managers/recordsHelper';

const BACKGROUND_JOB_KEY = 'backgroundLocationJob';

const USER_PROXIMITY_RADIUS = 0.1;
const NOTIFICATION_EXPIRATION = 24 * 60 * 60 * 1000;

var updateNearby;
var userUUID;

var startListening = function(location) {
  // Get users nearby
  if (!updateNearby) {
    // Get an realtime list of nearby [latitude, longitude] with radius in km
    updateNearby = GeofireWrapper.updateNearbyUsers(
      location.latitude,
      location.longitude,
      USER_PROXIMITY_RADIUS,
      enteredNearbyUser,
      exitedNearbyUser
    );
  } else {
    GeofireWrapper.modifyUpdateNearbyUsers(updateNearby, location.latitude, location.longitude, USER_PROXIMITY_RADIUS);
  }
};

var stopListening = function() {
  GeofireWrapper.cancelUpdate(updateNearby);
  updateNearby = null;
};

var enteredNearbyUser = function(key, location, distance) {
  // Ignore user's own uuid
  if (key === userUUID) {
    return;
  }

  // Get name from key
  Firebase.getInstance().getUser(key).then((user) => {
    let recordEntriesUnsubscribe = Firebase.getInstance().getEntriesForRecord(accountPairingKey(userUUID, user.id), async (pairingKey, entries) => {
      let { balance } = calculateBalance(userUUID, entries);
      if (!balance) {
        return;
      }

      // Balance positive means owed in
      let title;
      if (balance > 0) {
        title = user.name + ' owes you';
      } else {
        title = 'You owe ' + user.name;
      }

      // If the notifcation has expired, give notification
      let shouldNotify = false;
      let lastNotification = await Firebase.getInstance().getNotificationHistory(userUUID, user.id);
      if (lastNotification) {
        lastNotification = new Date(lastNotification);
        if (Date.now() - lastNotification.getTime() >= NOTIFICATION_EXPIRATION) {
          shouldNotify = true;
        }
        // else do not need to notify
      } else {
        shouldNotify = true;
      }
      // If there is not history of a notification, notify
      if (shouldNotify) {
        // Display a notification
        PushNotification.localNotification({
          title: title,
          message: 'Outstanding balance: ' + balance
        });
        // Track notifications by timestamp to avoid dupes
        Firebase.getInstance().setNotificationHistory(userUUID, user.id, new Date());
      }

      recordEntriesUnsubscribe();
    });
  });
};

var exitedNearbyUser = function(key, location, distance) {
  // Do nothing
};

var backgroundLocationTask = async (taskData) => {
  // Track the user's location
  let uuid = await PersistentStore.getUserID();
  if (uuid) {
    // Log background run time for debugging
    await Firebase.getInstance().setBackgroundTimestamp(uuid, new Date());

    userUUID = uuid;
    Locator.config();
    let location = await Locator.getCurrentLocation(false);
    await GeofireWrapper.updateLocation(uuid, [location.latitude, location.longitude]);
    startListening(location);
  } else {
    userUUID = null;
    stopListening();
  }
};

const backgroundLocationJob = {
  jobKey: BACKGROUND_JOB_KEY,
  job: backgroundLocationTask
};

export { BACKGROUND_JOB_KEY, backgroundLocationJob };
