const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

const { notifyUser } = require('./src/notifyUser');
exports.notifyUser = notifyUser;

const { cleanUsers } = require('./src/cleanUsers');
exports.cleanUsers = cleanUsers;

const { inviteAccepted } = require('./src/inviteAccepted');
exports.inviteAccepted = inviteAccepted;

const { updateBalance } = require('./src/updateBalance');
exports.updateBalance = updateBalance;

const { calculateBalance } = require('./src/calculateBalance');
exports.calculateBalance = calculateBalance;

const { updatePending } = require('./src/updatePending');
exports.updatePending = updatePending;
