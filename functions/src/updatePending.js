const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

exports.updatePending = functions.firestore.document('records/{pairingKey}/history/{recordKey}')
  .onWrite((change, context) => {
    const newEntry = change.after.data();

    let pending = false;
    if (!change.before.exists) {
      // New Entries must be pending
      pending = true;
    } else if (newEntry.cancelled) {
      // Cancelled Entries don't need to be approved anymore
      pending = false;
    } else {
      // If either user has not approved then it's pending
      pending = !newEntry.borrowerApproved || !newEntry.lenderApproved;
    }

    var recordRef = admin.firestore().collection('records').doc(context.params.pairingKey);

    return admin.firestore().runTransaction(t => {
      return t.get(recordRef)
        .then(doc => {
          t.update(recordRef, { pending });
      });
    });
  });
