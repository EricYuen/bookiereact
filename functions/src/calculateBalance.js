const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

// Parameter: pairingKey=xxxxxxxxxxx
exports.calculateBalance = functions.https.onRequest((req, res) => {
  var pairingKey = req.query.pairingKey;
  return admin.firestore().collection('records').doc(pairingKey).get().then(doc => {
    var user1 = doc.data().user1;

    var balance = 0;
    return admin.firestore().collection('records').doc(pairingKey).collection('history').get().then(snapshot => {
      snapshot.forEach(entryDoc => {
        var entry = entryDoc.data();

        if (entry.borrowerApproved && entry.lenderApproved && !entry.cancelled) {
          if (user1 === entry.borrower) {
            balance -= entry.amount;
          } else if (user1 === entry.lender) {
            balance += entry.amount;
          }
        }
      });

      admin.firestore().collection('records').doc(pairingKey).set({
        balance
      }, {merge: true});
    });
  });
});
