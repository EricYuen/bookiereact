const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

function listAllUsers(nextPageToken) {
  // List batch of users, 10 at a time.
  admin.auth().listUsers(10, nextPageToken)
    .then(function(listUsersResult) {
      listUsersResult.users.forEach(function(userRecord) {
        if (userRecord.isAnonymous) {
          admin.auth().deleteUser(userRecord.uid)
            .then(function() {
                console.log('Successfully deleted user');
            })
            .catch(function(error) {
                console.log('Error deleting user:', error);
            });
        }
      });
      if (listUsersResult.pageToken) {
        // List next batch of users.
        // Wait because timeout
        setTimeout(listAllUsers, 2000, listUsersResult.pageToken);
      }
    })
    .catch(function(error) {
      console.log('Error listing users:', error);
    });
}

exports.cleanUsers = functions.https.onRequest((req, res) => {
  listAllUsers();
});
