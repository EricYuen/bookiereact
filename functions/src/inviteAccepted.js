const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

let getUser = function (userId) {
  return new Promise((resolve, reject) => {
    admin.firestore().collection('users').doc(userId).get()
      .then(doc => {
        resolve(doc.data());
      }).catch((e) => {
        reject(e);
      });
  });
};

exports.inviteAccepted = functions.firestore.document('invites/{inviterKey}/invitees/{inviteeKey}')
  .onCreate((snap, context) => {
    // Get name of invitee
    return getUser(context.params.inviteeKey).then((invitee) => {
      if (!invitee) {
        return;
      }
      return getUser(context.params.inviterKey).then((inviter) => {
        if (!inviter) {
          return;
        }

        // Push notification to inviter's token
        let payload = {
          notification: {
            title: invitee.name + ' accepted your invitation.'
          }
        };

        admin.messaging().sendToDevice(inviter.token, payload);
      });
    });
  });
