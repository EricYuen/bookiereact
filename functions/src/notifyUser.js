const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

let getUser = function (userId) {
  return new Promise((resolve, reject) => {
    admin.firestore().collection('users').doc(userId).get()
      .then(doc => {
        resolve(doc.data());
      }).catch((e) => {
        reject(e);
      });
  });
};

let notifyUsersOfRecord = function(lender, borrower, record) {
  // Do not notify if the record has been cancelled
  if (record.cancelled) {
    return;
  }

  let payload = {
    notification: {
      title: 'Please Confirm'
    }
  };

  // Notify the borrower if they haven't approved
  if (!record.borrowerApproved) {
    payload.notification.body = 'You borrowed ' + record.amount + ' from ' + lender.name;
    admin.messaging().sendToDevice(borrower.token, payload);
  }

  // Notify the lender if they haven't approved
  if (!record.lenderApproved) {
    payload.notification.body = 'You lent ' + record.amount + ' to ' + borrower.name;
    admin.messaging().sendToDevice(lender.token, payload);
  }
};

exports.notifyUser = functions.firestore.document('records/{pairingKey}/history/{recordKey}')
  .onCreate((snap, context) => {
    const record = snap.data();

    // If both users have approved the record, do nothing
    if (record.borrowerApproved && record.lenderApproved) {
      return;
    }

    // Get the user models
    return getUser(record.lender).then((lender) => {
      return getUser(record.borrower).then((borrower) => {
        return notifyUsersOfRecord(lender, borrower, record);
      });
    });
  });
