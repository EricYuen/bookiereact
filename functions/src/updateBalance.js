const functions = require('firebase-functions');
const admin = require('firebase-admin');
try {admin.initializeApp();} catch (e) {}

exports.updateBalance = functions.firestore.document('records/{pairingKey}/history/{recordKey}')
  .onWrite((change, context) => {
    // If document was created
    if (!change.before.exists) {
      // Newly created entries will not be approved by both users, do not need to update the balance
      return null;
    }

    const oldEntry = change.before.data();
    const newEntry = change.after.data();

    // Only if borrower and lender both approved
    if (newEntry.borrowerApproved && newEntry.lenderApproved && !newEntry.cancelled) {
      // Only if previously one of them did not approved
      if (!oldEntry.borrowerApproved || !oldEntry.lenderApproved) {
        var recordRef = admin.firestore().collection('records').doc(context.params.pairingKey);

        return admin.firestore().runTransaction(t => {
          return t.get(recordRef)
            .then(doc => {
              var record = doc.data();
              if (record.balance == null) {
                record.balance = 0;
              }

              // Balance is always relative to User1
              // If borrower is the user1, the amount should be negative
              var balanceDelta = 0;
              if (record.user1 === newEntry.borrower) {
                balanceDelta = -newEntry.amount;
              } else if (record.user1 === newEntry.lender) {
                balanceDelta = newEntry.amount;
              }

              if (balanceDelta !== 0) {
                var newBalance = record.balance + balanceDelta;
                t.update(recordRef, { balance: newBalance });
              }
            });
        });
      }
    }
  });
