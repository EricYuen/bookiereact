/**
 * Index (Android)
 * @flow
 */

import {Navigation, NativeEventsReceiver} from 'react-native-navigation';
import BackgroundJob from 'react-native-background-job';

import * as Bookie from './app/Bookie';
import { backgroundLocationJob } from './app/BackgroundLocationJob';

BackgroundJob.register(backgroundLocationJob);

Promise.resolve(Navigation.isAppLaunched())
  .then(appLaunched => {
    if (appLaunched) {
      Bookie.launch();
    } else {
      new NativeEventsReceiver().appLaunched(Bookie.launch); // App hasn't been launched yet -> show the UI only when needed.
    }
  });
